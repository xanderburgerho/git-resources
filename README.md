# Git Resources

Table of contents:

[TOC]

## Learning Git

I have collected some resources to help you get started with Git.

### Basics

For reading about the basics of Git, I recommend the following resources:

- [Software Carpentry Git Tutorial](https://swcarpentry.github.io/git-novice/)
- [Gitlab Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Pro Git Book](https://git-scm.com/book/en/v2)

If you prefer video tutorials, I recommend the following:

- [Jessica Kerr - Git Happens!](https://youtu.be/Dv8I_kfrFWw) A bit old, but still relevant and a great introduction to git and how it works.

### Branching

Branching is a powerful feature of Git. To learn more about branching, I recommend the following resources:

- [Visualizing Git](http://git-school.github.io/visualizing-git/): Here you can visualize git commands and see how they affect the git tree
- [Learn Git Branching](https://learngitbranching.js.org/): An interactive tutorial to learn git branching

### Workshop

The TU Delft Digital Competence Center offers a [workshop on Git](https://www.tudelft.nl/en/library/research-data-management/r/training-events/training-for-researchers/version-control-collaborative-development-for-research-software)  every now and then.

## Git Installation

To install Git, choose your operating system and follow the instructions:

- [Windows](https://git-scm.com/download/win)
- [Mac](https://git-scm.com/download/mac)
- [Linux](https://git-scm.com/download/linux)

When you have a TU Delft laptop, you can also use the Software Center to install Git.

## Git Configuration

After installing Git, you need to configure your username and email. Run these commands in your terminal:

```bash
git config --global user.name "Your Name"
git config --global user.email "your-email@example.com"
```

## TU Delft Gitlab

The TU Delft has its own Gitlab server. You can access it by going to [https://gitlab.tudelft.nl](https://gitlab.tudelft.nl). You can log in with your NetID.

## Git Clients

The [command line](https://git-scm.com/docs) is the most powerful way to use Git, but it has a steep learning curve.
When using the command line, I recommend using a [shell](https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Bash) that supports Git, like [Git Bash](https://gitforwindows.org/) on windows (included in the default installation).
Also, I recommend customizing your [prompt](https://starship.rs/) to show the current branch and status.

If you prefer a graphical user interface (GUI) for Git, I recommend using one (or more) of the following clients:

- [Sourcetree](https://www.sourcetreeapp.com/) is a free Git client for Windows and Mac that supports Gitlab, Github and local repositories. Note that you don't need to create an Atlassian account to use Sourcetree, you can skip that step during installation.
- [GitHub Desktop](https://desktop.github.com/) (works with Gitlab and simple local repositories too)
- [Visual Studio Code](https://code.visualstudio.com/) with the GitLens extension or the Git Graph extension
- [gitk](https://git-scm.com/docs/gitk) and [git-gui](https://git-scm.com/docs/git-gui) are shipped with Git and provide a simple GUI for Git.
- [TortoiseGit](https://tortoisegit.org/) integrates with Windows Explorer

## IDE Integration

Most IDEs have Git integration. Here are some resources for the most popular IDEs:

- [Visual Studio Code](https://code.visualstudio.com/docs/editor/versioncontrol)
- [Matlab](https://nl.mathworks.com/help/matlab/source-control.html)
- [PyCharm](https://www.jetbrains.com/help/pycharm/set-up-a-git-repository.html)
- [CLion](https://www.jetbrains.com/help/clion/set-up-a-git-repository.html)

## Slides

I have created some slides to introduce you to Git:
[Introduction to Version Control with Git](https://gitlab.tudelft.nl/xanderburgerho/git-resources/-/blob/3e32ddf1f5d4246ea64eff36344b4c7046d72504/presentations/Introduction%20to%20Version%20Control.pptx)

These are the slides of the presentation I gave at TU Delft on May 31, 2024.
